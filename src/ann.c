#include <math.h>
#include "ann.h"

/* Helper functions for individual layers */
static AnnStatus layer_create(
    Layer ** pp_layer,
    size_t num_input,
    size_t num_output,
    const float * p_weight,
    const float * p_bias);

static void layer_release(
    Layer * p_layer);

static AnnStatus layer_compute(
    Layer * p_layer,
    size_t num_input,
    size_t num_output,
    const float * pf_input,
    float * pf_output);

/* Brief: Simple implementation of sigmoid function */
float sigmoid(float x)
{
    return 1.f/(1.f + exp(-x)); 
}

/* Brief: Create artificial neural network.
   Input:
   max_layers - Maximum number of layers.
   pp_ann - Pointer to a variable to fill with new ANN pointer.
   Output:
   *pp_ann - Should contain a pointer to a newly created ANN.
   Return:
   ANN_STATUS_OK - Operation completed successfully.
   ANN_STATUS_NULL_POINTER - Null pointer passed.
   ANN_STATUS_OUT_OF_RANGE - Parameter 'max_layers' is out of range.
   ANN_STATUS_OUT_OF_MEMORY - Not enough memory to allocate the ANN.
*/
AnnStatus ann_create(
    size_t max_layers,
    Ann ** pp_ann)
{
    if (NULL == pp_ann)
    {
	return ANN_STATUS_NULL_POINTER;
    }
    
    if (max_layers <= 0 ||
	max_layers > MAX_NUMBER_OF_LAYERS)
    {
	// memory is not allocated
	// need to set pointer to NULL to avoid
	// free on unallocated memory
	*pp_ann = NULL; 
	return ANN_STATUS_OUT_OF_RANGE;
    }

    Ann * p_ann = (Ann *)malloc(sizeof(Ann));

    if (NULL == p_ann)
    {
	return ANN_STATUS_OUT_OF_MEMORY;
    }

    size_t sz = max_layers * sizeof(Layer*);
    p_ann->pp_layers = (Layer **)malloc(sz);
    if (NULL == p_ann->pp_layers)
    {
	return ANN_STATUS_OUT_OF_MEMORY;
    }
    
    p_ann->n_max_layers = max_layers;
    p_ann->n_layers = 0;
    p_ann->n_max_size = 0;
    
    *pp_ann = p_ann;
    
    return ANN_STATUS_OK;
}

/* Brief: Destroy ANN and free all buffers.
   Input:
   pp_ann - A pointer to ANN pointer.
   Output:
   *pp_ann should be freed if non-null.
   *pp_ann should be set to null.
   Return:
   <none>
*/
void ann_release(
    Ann ** pp_ann)
{
    if (NULL == pp_ann)
    {
	return;
    }
    
    Ann * p_ann = *pp_ann;

    if (NULL == p_ann)
    {
	return;
    }

    for (size_t i = 0; i < p_ann->n_layers; ++i)
    {
	layer_release(p_ann->pp_layers[i]);
    }
    free(p_ann->pp_layers);
    free(p_ann);
    *pp_ann = NULL;
}


/* Brief: Add layer and copy layer data into internal structures.
   Input:
   p_ann - ANN pointer.
   num_input - Number of elements in input vector
   num_output - Number of elements in output vector
   p_weight - Weight matrix of size 'num_output' x 'num_input'.
   So it has 'num_output' rows and 'num_input' columns.
   Data is stored continuosly:
   W11, W12, ... W1n,
   W21, W22, ... W2n,
   ...
   Wm1, Wm2, ... Wmn

   where m = num_output and n = num_input
   p_bias - Bias vector. It has 'num_output' elements.
   Output:
   <none>
   Return:
   ANN_STATUS_OK - Operation completed successfully.
   ANN_STATUS_NULL_POINTER - Null pointer passed.
   ANN_STATUS_OUT_OF_RANGE - Number of input or output elements
   is out of range.
   ANN_STATUS_OUT_OF_MEMORY - Not enough memory to allocate buffer and copy
   weight matrix or bias vector.
   ANN_STATUS_MAX_LAYER_EXCEEDED - No more layers can be added.
   ANN_STATUS_INCOMPATIBLE - Layer input is incompatible with previous
   layer output (size mismatch).
*/
AnnStatus ann_add(
    Ann * p_ann,
    size_t num_input,
    size_t num_output,
    const float * pf_weight,
    const float * pf_bias)
{
    if (p_ann     == NULL ||
	pf_weight == NULL ||
	pf_bias   == NULL)
    {
	return ANN_STATUS_NULL_POINTER;
    }
    
    if (p_ann->n_layers >= p_ann->n_max_layers)
    {
	return ANN_STATUS_MAX_LAYER_EXCEEDED;
    }

    if (num_input  <= 0 ||
	num_output <= 0 ||
	num_input  > MAX_NUMBER_OF_INPUT_OR_OUTPUT ||
	num_output > MAX_NUMBER_OF_INPUT_OR_OUTPUT)
    {
	return ANN_STATUS_OUT_OF_RANGE;
    }
    
    if (p_ann->n_layers != 0)
    {
	size_t prev_layer_output =
	    p_ann->pp_layers[p_ann->n_layers - 1]->n_output;
	if (num_input != prev_layer_output)
	{
	    return ANN_STATUS_INCOMPATIBLE;
	}
    }

    AnnStatus status = layer_create(
	&p_ann->pp_layers[p_ann->n_layers],
	num_input,
	num_output,
	pf_weight,
	pf_bias);

    if (status == ANN_STATUS_OK)
    {
	p_ann->n_layers++;
	if (num_input > p_ann->n_max_size)
	{
	    p_ann->n_max_size = num_input;
	}
	if (num_output > p_ann->n_max_size)
	{
	    p_ann->n_max_size = num_output;
	}
    }
    // If not OK, ANN state is untouched, so just forward status
    
    return status;
}

/* Brief: Perform 'forward' operation.
   Input:
   p_ann - ANN pointer.
   num_input - Number of elements in input vector
   num_output - Number of elements in output vector
   p_input - Input vector
   p_output - Output vector
   Output:
   *p_output - should be filled with result of forward operation
   Return:
   ANN_STATUS_OK - Operation completed successfully.
   ANN_STATUS_NULL_POINTER - Null pointer passed.
   ANN_STATUS_OUT_OF_RANGE - Number of input or output elements
   is out of range.
   ANN_STATUS_OUT_OF_MEMORY - Not enough memory to allocate temporal buffer.
   ANN_STATUS_NO_LAYERS - No layers in the network. Call ann_add() first
   ANN_STATUS_INCOMPATIBLE - Input or output vector has incompatible size.
*/
AnnStatus ann_forward(
    Ann * p_ann,
    size_t num_input,
    size_t num_output,
    const float * pf_input,
    float * pf_output)
{
    if (p_ann     == NULL ||
	pf_input  == NULL ||
	pf_output == NULL)
    {
	return ANN_STATUS_NULL_POINTER;
    }
    
    if (p_ann->n_layers == 0)
    {
	return ANN_STATUS_NO_LAYERS;
    }
    
    if (num_input  <= 0 ||
	num_output <= 0 ||
	num_input  >  MAX_NUMBER_OF_INPUT_OR_OUTPUT ||
	num_output > MAX_NUMBER_OF_INPUT_OR_OUTPUT)
    {
	return ANN_STATUS_OUT_OF_RANGE;
    }

    /* Here we assume, that during addition of layers
       compatibility between input/output sizes is preserved,
       so we need to check only ANN input(the first layer input)
       and output (the last output buffer) buffers */
    if (num_input  != p_ann->pp_layers[0]->n_input ||
	num_output != p_ann->pp_layers[p_ann->n_layers - 1]->n_output)
    {
	return ANN_STATUS_INCOMPATIBLE;
    }
    
    AnnStatus status = ANN_STATUS_OK;

    /* To make code more concise two buffers are always allocated
       pf_in_buffer - holds inputs for each layer
       pf_out_buffer - holds outputs for each layer.
       This simple approach has overhead in case of
       simple ANNs with one or two layers. In the former case
       we do not need additional buffers at all, as we can use
       pf_input ans pf_output instead, in the latter case we can use
       single buffer only. Here, we assume, that usually ANN will have
       many layers.
       This additional buffers can be associated with each layer
       and preallocated during layer creation, but this approach
       may fail on devices with memory limitations and big number
       of layers. Another approach is to preallocate buffers with
       maximum size to hold the input/output for each layer.
       Here this straightforward approach is used */

    /* First preallocate buffers and copy input values there */
    size_t buffer_size = p_ann->n_max_size * sizeof(float);
    float * pf_in_buffer = (float*)malloc(buffer_size);
    
    if (NULL == pf_in_buffer)
    {
	return ANN_STATUS_OUT_OF_MEMORY;
    }
    memcpy(pf_in_buffer, pf_input, num_input*sizeof(float));

    float * pf_out_buffer = (float*)malloc(buffer_size);
    
    if (NULL == pf_out_buffer)
    {
	free(pf_in_buffer);
	return ANN_STATUS_OUT_OF_MEMORY;
    }
    
    /* Process all layers */
    for (size_t i = 0; i < p_ann->n_layers; ++i)
    {
	Layer * p_layer = p_ann->pp_layers[i];

	status = layer_compute(
	    p_layer,
	    p_layer->n_input,
	    p_layer->n_output,
	    pf_in_buffer,
	    pf_out_buffer);

	if (status != ANN_STATUS_OK)
	{
	    free(pf_in_buffer);
	    free(pf_out_buffer);
	    return status;
	}

	if (i != p_ann->n_layers - 1)
	{
	    /* Not last layer - pass output to the input
	       of the next layer, so just swap in and out buffers */
	    float * tmp = pf_in_buffer;
	    pf_in_buffer = pf_out_buffer;
	    pf_out_buffer = tmp;
	}
	else
	{
	    /* Last layer: copy to output buffer and free memory */
	    memcpy(pf_output, pf_out_buffer, num_output*sizeof(float));
	    free(pf_in_buffer);
	    free(pf_out_buffer);
	}
    }
    
    return ANN_STATUS_OK;
}

/*Brief: Creates layer and copy layer data into internal structures.
   Input:
   pp_layer - layer pointer.
   num_input - Number of elements in input vector
   num_output - Number of elements in output vector
   p_weight - Weight matrix of size 'num_output' x 'num_input'.
   So it has 'num_output' rows and 'num_input' columns.
   Data is stored continuosly:
   W11, W12, ... W1n,
   W21, W22, ... W2n,
   ...
   Wm1, Wm2, ... Wmn

   where m = num_output and n = num_input
   p_bias - Bias vector. It has 'num_output' elements.
   Output:
   <none>
   Return:
   ANN_STATUS_OK - Operation completed successfully.
   ANN_STATUS_NULL_POINTER - Null pointer passed.
   ANN_STATUS_OUT_OF_MEMORY - Not enough memory to allocate buffer and copy weight matrix or bias vector.  
 */
AnnStatus layer_create(
    Layer ** pp_layer,
    size_t num_input,
    size_t num_output,
    const float * pf_weight,
    const float * pf_bias)
{
    if (NULL == pp_layer)
    {
	return ANN_STATUS_NULL_POINTER;
    }
    
    /* Allocate memory for layer
       If out of memory - free already allocated memory */
    Layer * p_layer = (Layer *)malloc(sizeof(Layer));

    if (NULL == p_layer) {
	return ANN_STATUS_OUT_OF_MEMORY;
    }

    /* Memory is allocated, now copy parameters */
    size_t sz = num_output * sizeof(float);
  
    p_layer->pf_biases = (float *)malloc(sz);
    if (NULL == p_layer->pf_biases)
    {
	free(p_layer);
	return ANN_STATUS_OUT_OF_MEMORY;
    }
    memcpy(p_layer->pf_biases, pf_bias, sz);

    sz = num_output * num_input * sizeof(float);

    p_layer->pf_weights = (float *)malloc(sz);
    if (NULL == p_layer->pf_weights)
    {
	free(p_layer->pf_biases);
	free(p_layer);
	return ANN_STATUS_OUT_OF_MEMORY;
    }
    memcpy(p_layer->pf_weights, pf_weight, sz);
    
    /* Everything OK, set parameters and return */
    p_layer->n_input = num_input;
    p_layer->n_output = num_output;

    *pp_layer = p_layer;
    
    return ANN_STATUS_OK;
}

/* Brief: Release of layer memory
   Parameters: Layer* p_layer - pointer to deallocated layer
*/

void layer_release(
    Layer* p_layer)
{
    if (NULL == p_layer)
    {
	return;
    }
    if (NULL != p_layer->pf_weights) {
	free(p_layer->pf_weights);
    }
    if (NULL != p_layer->pf_biases) {
	free(p_layer->pf_biases);
    }
    free(p_layer);
}

/* Brief: Perform actual computations on layer
   It is assumed here that all checks are passed
   and data is consistent between layers.
   Parameters: 
   p_layer - pointer to layer,
   num_input - number of inputs,
   num_output - number of outputs,
   pf_input - pointer to memory with input values,
   pf_output - pointer to memory where to store output values
   Return values: ANN_STATUS_OK
*/
AnnStatus layer_compute(
    Layer* p_layer,
    size_t num_input,
    size_t num_output,
    const float * pf_input,
    float * pf_output
    )
{
    for (size_t i = 0; i < num_output; ++i)
    {
	pf_output[i] = p_layer->pf_biases[i];
	for (size_t j = 0; j < num_input; ++j)
	{
	    pf_output[i] +=
		pf_input[j] * p_layer->pf_weights[j + i * num_input];
	}
	pf_output[i] = sigmoid(pf_output[i]);
    }
    return ANN_STATUS_OK;
}

