#include <stdio.h>

#include "ann.h"
#include "../test/test.h"

int main() {
    fprintf(stdout, "Simple ANN module\n");
    RunAllTests();
    return 0;
}
