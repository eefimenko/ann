# Simple ANN module with forward operation

This code implements simple Artificial Neural Net with forward operation in plain C (C99 standard)

# Interface

`AnnStatus` -- return status of performed operation

Possible values:

`ANN_STATUS_OK` -- operation successful  
`ANN_STATUS_NULL_POINTER` -- NULL pointer was passed as one of argument  
`ANN_STATUS_OUT_OF_RANGE` -- number of input or output elements is out of range  
`ANN_STATUS_OUT_OF_MEMORY`-- memory allocation failed  
`ANN_STATUS_MAX_LAYER_EXCEEDED` -- number of layers exceeds maximum value  
`ANN_STATUS_NO_LAYERS` -- ANN is empty  
`ANN_STATUS_INCOMPATIBLE` -- number of inputs of being added layer does not match number of outputs of previous layer 

`struct Ann` -- basic structure which represents ANN

## Create ANN
`AnnStatus ann_create(size_t max_layers,
    Ann ** pp_ann)` -- Create artificial neural network.  
   Input:  
   max_layers - Maximum number of layers.  
   pp_ann - Pointer to a variable to fill with new ANN pointer.  
   Output:  
   *pp_ann - Should contain a pointer to a newly created ANN.  
   Return:  
   ANN_STATUS_OK - Operation completed successfully.  
   ANN_STATUS_NULL_POINTER - Null pointer passed.  
   ANN_STATUS_OUT_OF_RANGE - Parameter 'max_layers' is out of range.  
   ANN_STATUS_OUT_OF_MEMORY - Not enough memory to allocate the ANN.  
   
## Add layer
`AnnStatus ann_add(
    Ann * p_ann,
    size_t num_input,
    size_t num_output,
    const float * p_weight,
    const float * p_bias)` -- Add layer and copy layer data into internal structures.  
   Input:  
   p_ann - ANN pointer.  
   num_input - Number of elements in input vector  
   num_output - Number of elements in output vector  
   p_weight - Weight matrix of size 'num_output' x 'num_input'.  
   So it has 'num_output' rows and 'num_input' columns.  
   Data is stored continuosly:  
   W11, W12, ... W1n,  
   W21, W22, ... W2n,  
   ...  
   Wm1, Wm2, ... Wmn  

   where m = num_output and n = num_input  
   p_bias - Bias vector. It has 'num_output' elements.  
   Output:  
   <none>  
   Return:  
   ANN_STATUS_OK - Operation completed successfully.  
   ANN_STATUS_NULL_POINTER - Null pointer passed.  
   ANN_STATUS_OUT_OF_RANGE - Number of input or output elements  
   is out of range.  
   ANN_STATUS_OUT_OF_MEMORY - Not enough memory to allocate buffer and copy  
   weight matrix or bias vector.  
   ANN_STATUS_MAX_LAYER_EXCEEDED - No more layers can be added.  
   ANN_STATUS_INCOMPATIBLE - Layer input is incompatible with previous  
   layer output (size mismatch).  
   
## Forward pass
`AnnStatus ann_forward(
    Ann * p_ann,
    size_t num_input,
    size_t num_output,
    const float * p_input,
    float * p_output)` -- Perform 'forward' operation.  
   Input:  
   p_ann - ANN pointer.  
   num_input - Number of elements in input vector  
   num_output - Number of elements in output vector  
   p_input - Input vector  
   p_output - Output vector  
   Output:  
   *p_output - should be filled with result of forward operation  
   Return:  
   ANN_STATUS_OK - Operation completed successfully.  
   ANN_STATUS_NULL_POINTER - Null pointer passed.  
   ANN_STATUS_OUT_OF_RANGE - Number of input or output elements  
   is out of range.  
   ANN_STATUS_OUT_OF_MEMORY - Not enough memory to allocate temporal buffer.  
   ANN_STATUS_NO_LAYERS - No layers in the network. Call ann_add() first  
   ANN_STATUS_INCOMPATIBLE - Input or output vector has incompatible size.  

## ANN release
`void ann_release(
    Ann ** pp_ann)` -- Destroy ANN and free all buffers.  
   Input:  
   pp_ann - A pointer to ANN pointer.  
   Output:  
   *pp_ann should be freed if non-null.  
   *pp_ann should be set to null.  
   Return:  
   <none>  
   
# How to install  

This code can be built using `cmake`  
`main.c` runs simple tests covering basic functionality

- Clone this repository
- `cd ANN`
- `mkdir build`
- `cd build`
- `cmake ..`
- `make`
- `./main`

# Example of usage

The usage of developed module for a creation of a simple 3-layer ANN is demonstrated below  

    Ann* p_ann;
    AnnStatus status = ann_create(10, &p_ann); // create ANN
       
    float f_weights[] = {1, 2, 3, 4, 5, 6}; // array of weights
    float f_biases[] = {1, 2, 3}; // array of biases
    
    status = ann_add(p_ann,
		     2, // num_input
		     3, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases); // add first layer
    
    status = ann_add(p_ann,
		     3, // num_input
		     2, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases); // add second layer
    
    status = ann_add(p_ann,
		     2, // num_input
		     3, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases); // add third layer
        
    float f_input[] = {1, 2}; // input
    float f_output[] = {0, 0, 0}; //placeholder for output
    
    status = ann_forward(
	         p_ann,
	         2, // num_input
	         3, // num_output
	         (const float *)&f_input,
	         (float *)&f_output); // perform forward pass
         
    ann_release(&p_ann); // release ANN
