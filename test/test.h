#ifndef TEST_FOR_ANN
#define TEST_FOR_ANN

#include "ann.h"

void ann_print(
    Ann * p_ann);

void layer_print(
    Layer * p_layer);

void RunAllTests();

#endif
