#include <stdio.h>
#include <assert.h>
#include "ann.h"
#include "test.h"

void NetFromNullPointer(const char* message) {
    Ann** pp_ann = NULL;
    AnnStatus status = ann_create(10, pp_ann);
    assert(status == ANN_STATUS_NULL_POINTER);
    printf("%s: OK\n", message);
}

void NetWithTooManyLayersI(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(1025, &p_ann);
    assert(status == ANN_STATUS_OUT_OF_RANGE);
    
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void AddLayerToNullPointerNet(const char* message) {
    Ann* p_ann = NULL;
    float f_weights[] = {1, 2, 3, 4};
    float f_biases[] = {1, 2};

    AnnStatus status = ann_add(p_ann,
			       2, // num_input
			       2, // num_output
			       (const float *)&f_weights,
			       (const float *)&f_biases);
    assert(status == ANN_STATUS_NULL_POINTER);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void AddLayerToNetWithNullPointerWeights(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(3, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    float f_weights[] = {1, 2, 3, 4};
    float f_biases[] = {1, 2};

    status = ann_add(p_ann,
		     2, // num_input
		     2, // num_output
		     NULL,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_NULL_POINTER);
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void AddLayerToNetWithNullPointerBiases(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(3, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    float f_weights[] = {1, 2, 3, 4};
    float f_biases[] = {1, 2};

    status = ann_add(p_ann,
		     2, // num_input
		     2, // num_output
		     (const float *)&f_weights,
		     NULL);
    assert(status == ANN_STATUS_NULL_POINTER);
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void AddLayerToNetWithTooLargeInput(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(3, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    float f_weights[] = {1, 2, 3, 4};
    float f_biases[] = {1, 2};

    status = ann_add(p_ann,
		     2048, // num_input
		     2, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OUT_OF_RANGE);
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void NetWithTooManyLayersII(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(3, &p_ann);
    float f_weights[] = {1, 2, 3, 4};
    float f_biases[] = {1, 2};

    for (int i = 0; i < 3; ++i) {
	status = ann_add(p_ann,
			 2, // num_input
			 2, // num_output
			 (const float *)&f_weights,
			 (const float *)&f_biases);
	assert(status == ANN_STATUS_OK);
    }
    status = ann_add(p_ann,
		     2, // num_input
		     2, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_MAX_LAYER_EXCEEDED);
    
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void NetWithIncompatibleLayers(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(10, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    float f_weights[] = {1, 2, 3, 4, 5, 6};
    float f_biases[] = {1, 2, 3};
    status = ann_add(p_ann,
		     2, // num_input
		     3, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OK);
    
    status = ann_add(p_ann,
		     2, // num_input
		     2, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_INCOMPATIBLE);
    
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void EmptyNet(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(10, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void EmptyNetForward(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(10, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    float f_input[] = {1, 2};
    float f_output[] = {0, 0, 0};
    
    status = ann_forward(
	p_ann,
	2, // num_input
	3,
	(const float *)&f_input,
	(float *)&f_output);
    
    assert(status == ANN_STATUS_NO_LAYERS);
    
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void NetWithTwoLayersIncompatibleInput(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(10, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    float f_weights[] = {1, 2, 3, 4, 5, 6};
    float f_biases[] = {1, 2, 3};
    status = ann_add(p_ann,
		     2, // num_input
		     3, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OK);
    
    status = ann_add(p_ann,
		     3, // num_input
		     2, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OK);

    float f_input[] = {1, 2, 3};
    float f_output[] = {0, 0};
    
    status = ann_forward(
	p_ann,
	3, // num_input
	2,
	(const float *)&f_input,
	(float *)&f_output);
    assert(status == ANN_STATUS_INCOMPATIBLE);
    
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void NetWithTwoLayersIncompatibleOutput(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(10, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    float f_weights[] = {1, 2, 3, 4, 5, 6};
    float f_biases[] = {1, 2, 3};
    status = ann_add(p_ann,
		     2, // num_input
		     3, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OK);
    
    status = ann_add(p_ann,
		     3, // num_input
		     2, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OK);

    float f_input[] = {1, 2, 3};
    float f_output[] = {0, 0};
    
    status = ann_forward(
	p_ann,
	2, // num_input
	3,
	(const float *)&f_input,
	(float *)&f_output);
    assert(status == ANN_STATUS_INCOMPATIBLE);
    
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void NetWithSingleLayer(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(10, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    float f_weights[] = {1, 2, 3, 4, 5, 6};
    float f_biases[] = {1, 1, 1};
    status = ann_add(p_ann,
		     2, // num_input
		     3, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    //ann_print(p_ann);
    assert(status == ANN_STATUS_OK);

    float f_input[] = {1, 2};
    float f_output[] = {0, 0, 0};
    
    status = ann_forward(
	p_ann,
	2, // num_input
	3,
	(const float *)&f_input,
	(float *)&f_output);
    assert(status == ANN_STATUS_OK);
    	
    /*fprintf(stdout, "Result:");
    for (int i = 0; i < 3; ++i)
    {
	fprintf(stdout, "%f ", f_output[i]);
    }
    fprintf(stdout, "\n");*/
    
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void NetWithTwoLayers(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(10, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    float f_weights[] = {1, 2, 3, 4, 5, 6};
    float f_biases[] = {1, 2, 3};
    status = ann_add(p_ann,
		     2, // num_input
		     3, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OK);
    
    status = ann_add(p_ann,
		     3, // num_input
		     2, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OK);

    float f_input[] = {1, 2};
    float f_output[] = {0, 0};
    
    status = ann_forward(
	p_ann,
	2, // num_input
	2,
	(const float *)&f_input,
	(float *)&f_output);
    assert(status == ANN_STATUS_OK);
    	
    /*fprintf(stdout, "Result:");
    for (int i = 0; i < 2; ++i)
    {
	fprintf(stdout, "%f ", f_output[i]);
    }
    fprintf(stdout, "\n");*/
    
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void NetWithThreeLayers(const char* message) {
    Ann* p_ann;
    AnnStatus status = ann_create(10, &p_ann);
    assert(status == ANN_STATUS_OK);
    
    float f_weights[] = {1, 2, 3, 4, 5, 6};
    float f_biases[] = {1, 2, 3};
    
    status = ann_add(p_ann,
		     2, // num_input
		     3, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OK);
    
    status = ann_add(p_ann,
		     3, // num_input
		     2, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OK);

    status = ann_add(p_ann,
		     2, // num_input
		     3, // num_output
		     (const float *)&f_weights,
		     (const float *)&f_biases);
    assert(status == ANN_STATUS_OK);
    //ann_print(p_ann);
    
    float f_input[] = {1, 2};
    float f_output[] = {0, 0, 0};
    
    status = ann_forward(
	p_ann,
	2, // num_input
	3, // num_output
	(const float *)&f_input,
	(float *)&f_output);
    assert(status == ANN_STATUS_OK);
    	
    /*fprintf(stdout, "Result:");
    for (int i = 0; i < 3; ++i)
    {
	fprintf(stdout, "%f ", f_output[i]);
    }
    fprintf(stdout, "\n");*/
    
    ann_release(&p_ann);
    assert(p_ann == NULL);
    printf("%s: OK\n", message);
}

void RunAllTests() {
    NetFromNullPointer("NetFromNullPointer");
    NetWithTooManyLayersI("NetWithTooManyLayersI");
    NetWithTooManyLayersII("NetWithTooManyLayersII");
    AddLayerToNullPointerNet("AddLayerToNullPointerNet");
    AddLayerToNetWithNullPointerWeights("AddLayerToNetWithNullPointerWeights");
    AddLayerToNetWithNullPointerBiases("AddLayerToNetWithNullPointerWeights");
    AddLayerToNetWithTooLargeInput("AddLayerToNetWithTooLargeInput");
    NetWithIncompatibleLayers("NetWithIncompatibleLayers");
    NetWithTwoLayersIncompatibleInput("NetWithTwoLayersIncompatibleInput");
    NetWithTwoLayersIncompatibleOutput("NetWithTwoLayersIncompatibleOutput");
    EmptyNet("EmptyNet");
    EmptyNetForward("EmptyNetForward");
    NetWithSingleLayer("NetWithSingleLayer");
    NetWithTwoLayers("NetWithTwoLayers");
    NetWithThreeLayers("NetWithTnreeLayers");
}

/* Helper functions for debugging */
/* Brief: Prints out description of ANN */
void ann_print(
    Ann * p_ann)
{
    fprintf(stdout, "ANN pointer: %p\n", p_ann);
    fprintf(stdout, "Max number of layers: %ld\n", p_ann->n_max_layers);
    fprintf(stdout, "Number of layers: %ld\n", p_ann->n_layers);

    for (size_t i = 0; i < p_ann->n_layers; ++i)
    {
	fprintf(stdout, "Layer %lu:\n", i+1);
	layer_print(p_ann->pp_layers[i]);
	fprintf(stdout, "\n");
    }
}

/* Brief: Prints out description of ANN layer */
void layer_print(
    Layer * p_layer)
{
    fprintf(stdout, "Num inputs: %ld\n", p_layer->n_input);
    fprintf(stdout, "Num outputs: %ld\n", p_layer->n_output);
    fprintf(stdout, "Weights:\n");
    for (size_t j = 0; j < p_layer->n_output; ++j)
    {
	for (size_t i = 0; i < p_layer->n_input; ++i)
	{
	    fprintf(stdout,
		    "%f ",
		    p_layer->pf_weights[i + j * p_layer->n_input]);
	}
	fprintf(stdout, "\n");
    }
    fprintf(stdout, "Biases:\n");
    for (size_t j = 0; j < p_layer->n_output; ++j)
    {
	fprintf(stdout,
		"%f ",
		p_layer->pf_biases[j]);
    }
    fprintf(stdout, "\n");
}
